# Activity 3
### Group Members:

* **Manjit Singh (C0744018)**


* **Prabhjinder Singh (C0740758)**


---

*  Test Case 1

   To match the name of the field to be "Subscribe to My McD" is **Working Successfully**.

*  Test Case 2

   Input values in the fields and then open **Captcha** by clicking on the subscribe button is done. Tried to press the verify button on Captcha but it do not work.

*  Test Case 3

   Fields are empty and on pressing the subscribe button, **captcha** is opened. Tried to press the verify button on Captcha but it do not work. 

---