import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity {
	
	// location of chrome driver file location
	final String CHROMEDRIVER_LOCATION = "/Users/macstudent/Desktop/chromedriver";
	final String URL_TO_TEST = "https://www.mcdonalds.com/ca/en-ca.html";
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		
		//selenium setup
		// Selenium + Chrome
		 		System.setProperty("webdriver.chrome.driver", 
		 				CHROMEDRIVER_LOCATION);
		 		driver = new ChromeDriver();
		 		
		 		// 2. go to the website
		 		driver.get(URL_TO_TEST);
		 		
		 		// close the advertisement by pressing the cross button
		 		WebElement showMessageButton = driver.findElement(By.cssSelector("a.exit"));
				// -push the button
				showMessageButton.click();
	}

	@After
	public void tearDown() throws Exception {
		
		//close the browser
		Thread.sleep(5000);
		driver.close();
	}
// test case 1
	@Test
	public void testSubscribeToMyMcD() {
		
		// get the expected value 
		String expectedValue = driver.findElement(By.className("click-before-outline")).getText();

// actual value
		String actualValue = "Subscribe to My McD’s®";

		System.out.println("The Original value: "+ expectedValue);

		
// compare the expected and actual results
		assertEquals(expectedValue, actualValue);
	}
	
	// test case 2

		@Test

		public void testEmailSignUp() {

			
			// type first name in input
			WebElement inputFirstName = driver.findElement(By.id("firstname2"));

	 		inputFirstName.sendKeys("Prabhjinder");
	 		
	 		// type email address in input

	 		WebElement inputEmailAddress = driver.findElement(By.id("email2"));

	 		inputEmailAddress.sendKeys("Singh");
	 		
	 		// type first 3 characters of postal code

	 		WebElement inputPostalCode = driver.findElement(By.id("postalcode2"));

	 		inputPostalCode.sendKeys("M1W");

			 		
          // get the subscribe button
	 		WebElement subscribeButton = driver.findElement(By.cssSelector("button#g-recaptcha-btn-2"));

			// -push the button

			subscribeButton.click();
			
			// press rechaptcha verify button
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement verifyButton = driver.findElement(By.cssSelector("recaptcha-verify-button"));
		
			// -push the button
			verifyButton.click();

		}
		
		// test case 3

		
		@Test

		public void testEmailSignUpFailed() {

			WebElement subscribeButton = driver.findElement(By.cssSelector("button#g-recaptcha-btn-2"));

			// -push the button

			subscribeButton.click();
			
			// push captcha verify button
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement vButton1 = driver.findElement(By.cssSelector("#recaptcha-verify-button"));

		//	 -push the button
			vButton1.click(); 

			

		}

		
	

	
	

}
